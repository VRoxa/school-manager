﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Commons
{
    public class Constants
    {
        public static readonly string SYMMETRIC_SECURITY_KEY = @"SecurityKeyForAtLeast16BytesLength";
        public static readonly string VALID_AUDIENCE = @"auth@schoolmanagement";
    }
}
