﻿using Data.Access.Layer.Client;
using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Generic;
using Data.Access.Layer.Repositories.Interfaces;
using School.Manager.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Access.Layer.Repositories.Implementations
{
    public class UserManager : GenericRepository<UserAuthentication, string>, IUserManager
    {
        private readonly ILog<UserManager> _log;

        public UserManager(IMongoDb<UserAuthentication> database, ILog<UserManager> log) : base(database) 
        {
            _log = log;
        }

        public bool CheckValid(UserAuthentication user, string password)
        {
            return user.Password == password;
        }

        public async Task<UserAuthentication> GetUser(string username)
        {
            IEnumerable<UserAuthentication> users = await GetAll();
            UserAuthentication user = users.FirstOrDefault(u => u.Username == username);

            if (user is null)
            {
                throw new ArgumentException($"{username}: User not found");
            }

            return user;
        }

        public async Task LogInUser(UserAuthentication user)
        {
            user.LastSession = DateTime.Now;
            await Update(user);
            _log.Info($"User {user.Username} logged in");
        }

        public async Task RegisterUser(UserAuthentication user)
        {
            user.CreationDate = DateTime.Now;
            await Create(user);
            _log.Info($"User {user.Username} registered");
        }
    }
}
