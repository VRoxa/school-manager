﻿using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Generic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Access.Layer.Repositories.Interfaces
{
    public interface IUserManager : IGenericRepository<UserAuthentication, string>
    {
        Task<UserAuthentication> GetUser(string username);
        Task RegisterUser(UserAuthentication user);
        Task LogInUser(UserAuthentication user);
        bool CheckValid(UserAuthentication user, string password);
    }
}
