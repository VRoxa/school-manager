﻿using Data.Access.Layer.Client;
using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Generic;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Access.Layer.Repositories.Generic
{
    public class GenericRepository<TEntity, TIdentifier> : IGenericRepository<TEntity, TIdentifier> where TEntity : IMongoEntity<TIdentifier>
    {
        private readonly IMongoCollection<TEntity> _entities;

        public GenericRepository(IMongoDb<TEntity> db)
        {
            string collection = $"{typeof(TEntity).Name}Collection";
            _entities = db.GetCollection(collection);
        }

        public async Task<TEntity> Create(TEntity entity)
        {
            await _entities.InsertOneAsync(entity);
            return entity;
        }

        public async Task<TEntity> Get(TIdentifier id)
        {
            TEntity entity = (await _entities.FindAsync<TEntity>(EqualsId(id))).FirstOrDefault();
            return entity;
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            IEnumerable<TEntity> entities = (await _entities.FindAsync<TEntity>(e => true)).ToEnumerable();
            return entities;
        }

        public async Task<IEnumerable<TEntity>> GetAll(Expression<Func<TEntity, bool>> predicate)
        {
            IEnumerable<TEntity> entities = (await _entities.FindAsync<TEntity>(predicate)).ToEnumerable();
            return entities;
        }

        public async Task Remove(TEntity entity)
        {
            await Remove(entity.Id);
        }

        public async Task Remove(TIdentifier id)
        {
            await _entities.DeleteOneAsync<TEntity>(EqualsId(id));
        }

        public async Task<long> Remove(Expression<Func<TEntity, bool>> predicate)
        {
            DeleteResult result = await _entities.DeleteManyAsync<TEntity>(predicate);
            return result.DeletedCount;
        }

        public async Task Update(TEntity entity)
        {
            await Update(entity.Id, entity);
        }

        public async Task Update(TIdentifier id, TEntity entity)
        {
            await _entities.ReplaceOneAsync<TEntity>(EqualsId(id), entity);
        }

        private Expression<Func<TEntity, bool>> EqualsId(TIdentifier id)
        {
            return e => e.Id.Equals(id);
        }
    }
}
