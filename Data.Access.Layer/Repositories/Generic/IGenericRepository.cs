﻿using Data.Access.Layer.Models.Generic;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data.Access.Layer.Repositories.Generic
{
    public interface IGenericRepository<TEntity, TIdentifier>
    {
        Task<TEntity> Create(TEntity entity);
        Task<TEntity> Get(TIdentifier id);
        Task<IEnumerable<TEntity>> GetAll();
        Task<IEnumerable<TEntity>> GetAll(Expression<Func<TEntity, bool>> predicate);
        Task Remove(TEntity entity);
        Task Remove(TIdentifier id);
        Task<long> Remove(Expression<Func<TEntity, bool>> predicate);
        Task Update(TEntity entity);
        Task Update(TIdentifier id, TEntity entity);
    }
}
