﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models
{
    public class Address
    {
        public string PostalAddress { get; set; }
        public string PostalCode { get; set; }
    }
}
