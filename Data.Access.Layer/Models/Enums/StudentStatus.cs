﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models.Enums
{
    public enum StudentStatus
    {
        Subscribed,
        Unsubscribed
    }
}
