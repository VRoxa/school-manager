﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models
{
    public class Contact
    {
        public string PhoneNumber { get; set; }
        public string CellphoneNumber { get; set; }
        public string Email { get; set; }
    }
}
