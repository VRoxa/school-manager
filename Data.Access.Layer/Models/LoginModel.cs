﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models
{
    public class LoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
