﻿using Data.Access.Layer.Models.Enums;
using Data.Access.Layer.Models.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models.Database
{
    public class Student : IMongoEntity<string>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string LastName { get; set; }
        public string DNI { get; set; }
        public DateTime Birthdate { get; set; }

        public Address Address { get; set; }
        public Contact Contact { get; set; }

        public PaymentData PaymentData { get; set; }
        public StudentStatus Status { get; set; }

        public string About { get; set; }

        public List<DateTime> Subscriptions { get; set; }
        public List<DateTime> Unsubscriptions { get; set; }
        public DateTime InsertTime { get; set; }
        public DateTime LastModified { get; set; }

        public List<Receipt> Receipts { get; set; }
    }
}
