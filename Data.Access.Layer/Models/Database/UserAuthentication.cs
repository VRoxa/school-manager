﻿using Data.Access.Layer.Models.Generic;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models.Database
{
    public class UserAuthentication : IMongoEntity<string>
    {
        [BsonRequired]
        public string Username { get; set; }
        [BsonRequired]
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastSession { get; set; }
    }
}
