﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models.Database
{
    public class Receipt
    {
        public DateTime ReceiptDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public double Amount { get; set; }

        public PaymentData PaymentData { get; set; }

        public string About { get; set; }
    }
}
