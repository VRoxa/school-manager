﻿using Data.Access.Layer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models
{
    public class PaymentData
    {
        public string BankAccount { get; set; }
        public PaymentType PaymentType { get; set; }
    }
}
