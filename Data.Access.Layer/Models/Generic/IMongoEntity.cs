﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Models.Generic
{
    public abstract class IMongoEntity<TIdentifier>
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public TIdentifier Id { get; set; }
    }
}
