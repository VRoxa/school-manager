﻿using Data.Access.Layer.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Access.Layer.Client
{
    public interface IMongoDb<TEntity>
    {
        IMongoCollection<TEntity>  GetCollection(string collection);
    }

    public class MongoDb<TEntity> : IMongoDb<TEntity>
    {
        private readonly IMongoDatabase _database;

        public MongoDb(IDatabaseSettings settings)
        {
            MongoClient client = new MongoClient(settings.ConnectionString);
            _database = client.GetDatabase(settings.DatabaseName);
        }

        public IMongoCollection<TEntity> GetCollection(string collection) => _database.GetCollection<TEntity>(collection);
    }
}
