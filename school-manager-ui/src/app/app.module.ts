import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Translation
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Included modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './modules/app-routing/app-routing.module';
import { MaterialModule } from './modules/material/material.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Components
import { LoginComponent } from './components/login/login.component';
import { LoaderComponent } from './components/login/loader/loader.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { StudentsComponent } from './components/students/students.component';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorTranslator } from './translation/mat-paginator-intl';
import { StudentCreationDialogComponent } from './components/students/student-creation-dialog/student-creation-dialog.component';

@NgModule({
  declarations: [
    LoginComponent,
    LoaderComponent,
    MainViewComponent,
    StudentsComponent,
    StudentCreationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http),
        deps: [HttpClient]
      }
    })
  ],
  entryComponents: [
    StudentCreationDialogComponent
  ],
  providers: [
    {
      provide: MatPaginatorIntl, 
      deps: [TranslateService],
      useFactory: (translateService: TranslateService) => new MatPaginatorTranslator(translateService)
    }
  ],
  bootstrap: [MainViewComponent]
})
export class AppModule { }
