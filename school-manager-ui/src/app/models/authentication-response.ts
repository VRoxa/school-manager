export class AuthenticationResponse {
    token: string;
    expiration: any;
}