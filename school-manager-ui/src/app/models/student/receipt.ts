import { PaymentData } from './payment-data';

export class Receipt {
    receiptDate: Date;
    paymentDate: Date;
    amount: number;

    paymentData: PaymentData;

    about: string;
}