export enum StudentStatus {
    Subscribed,
    Unsubscribed
}