import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentCreationDialogComponent } from './student-creation-dialog.component';

describe('StudentCreationDialogComponent', () => {
  let component: StudentCreationDialogComponent;
  let fixture: ComponentFixture<StudentCreationDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentCreationDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
