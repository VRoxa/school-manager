import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  @Input() stop: ReplaySubject<void>;
  @Output() animationEnd: EventEmitter<void> = new EventEmitter<void>();
  toStop = false;

  constructor() { }

  ngOnInit() {
    this.stop.subscribe(() => {
      this.toStop = true;

      setTimeout(() => {
        this.animationEnd.emit();
      }, 1000);
    });
  }

}
