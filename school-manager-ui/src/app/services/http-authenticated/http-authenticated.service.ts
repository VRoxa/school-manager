import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpAuthenticatedService {
  private static headers: HttpHeaders;
  private static onAuthenticated: ReplaySubject<void> = new ReplaySubject<void>();
  private _api: string;
  private _headers: HttpHeaders;


  constructor(private http: HttpClient) { 
    this._headers = HttpAuthenticatedService.headers;
    this._api = `${environment.api}/api`;
  }

  public get = <TResponse>(url: string): Observable<TResponse> => {
    return this.http.get<TResponse>(`${this._api}/${url}`, { headers: this._headers });
  }

  public post = <TResponse>(url: string, body?: any): Observable<TResponse> => {
    return this.http.post<TResponse>(`${this._api}/${url}`, body, { headers: this._headers });
  }

  public put = <TResponse>(url: string, body?: any): Observable<TResponse> => {
    return this.http.put<TResponse>(`${this._api}/${url}`, body, { headers: this._headers });
  }

  public patch = <TResponse>(url: string, body?: any): Observable<TResponse> => {
    return this.http.patch<TResponse>(`${this._api}/${url}`, body, { headers: this._headers });
  }

  public delete = <TResponse>(url: string): Observable<TResponse> => {
    return this.http.delete<TResponse>(`${this._api}/${url}`, { headers: this._headers });
  }

  public setToken = (token: string): void => {
    this._headers = HttpAuthenticatedService.headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    });

    HttpAuthenticatedService.onAuthenticated.next();
  }

  public isAuthenticated = (): boolean => {
    return this._headers !== undefined && this._headers !== null;
  }

  get onAuthenticated(): ReplaySubject<void> {
    return HttpAuthenticatedService.onAuthenticated;
  }

}
