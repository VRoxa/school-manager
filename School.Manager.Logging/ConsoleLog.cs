﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace School.Manager.Logging
{
    public class GenericLog<T> : ILog<T> where T : class
    {
        private readonly ILog _log;

        public GenericLog() => _log = new ConsoleLog(typeof(T).Name);

        public void Debug(string message) => _log.Debug(message);

        public void Error(string message) => _log.Error(message);

        public void Error(string message, Exception ex) => _log.Error(message, ex);

        public void Info(string message) => _log.Info(message);

        public void Verbose(string message) => _log.Verbose(message);

        public void Warn(string message) => _log.Warn(message);
        public void Fatal(string message) => _log.Fatal(message);
        public void Fatal(string message, Exception ex) => _log.Fatal(message, ex);
    }

    public class ConsoleLog : ILog
    {
        private readonly string _name;

        public ConsoleLog(string name)
        {
            _name = name;
        }

        public void Verbose(string message)
        {
            ForegroundColor = ConsoleColor.Gray;
            WriteLine(Format(message));
            Reset();
        }

        public void Debug(string message)
        {
            ForegroundColor = ConsoleColor.DarkGray;
            WriteLine(Format(message));
            Reset();
        }

        public void Error(string message)
        {
            ForegroundColor = ConsoleColor.DarkRed;
            WriteLine(Format(message));
            Reset();
        }

        public void Error(string message, Exception ex)
        {
            ForegroundColor = ConsoleColor.DarkRed;
            WriteLine(FormatException(message, ex));
            Reset();
        }

        public void Info(string message)
        {
            ForegroundColor = ConsoleColor.White;
            WriteLine(Format(message));
            Reset();
        }

        public void Warn(string message)
        {
            ForegroundColor = ConsoleColor.DarkYellow;
            WriteLine(Format(message));
            Reset();
        }

        private string Format(string message) => $"[{DateTime.Now:HH:mm:ss} {_name}] {message}";
        private string FormatException(string message, Exception e) => $"{Format(message)}\n{e.Message}:\n{e.StackTrace}";

        private void Reset() => ResetColor();

        public void Fatal(string message)
        {
            ForegroundColor = ConsoleColor.DarkMagenta;
            WriteLine(Format(message));
            Reset();
        }

        public void Fatal(string message, Exception ex)
        {
            ForegroundColor = ConsoleColor.DarkMagenta;
            WriteLine(FormatException(message, ex));
            Reset();
        }
    }
}
