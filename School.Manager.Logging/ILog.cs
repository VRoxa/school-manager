﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School.Manager.Logging
{
    public interface ILog
    {
        void Verbose(string message);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Error(string message, Exception ex);
        void Fatal(string message);
        void Fatal(string message, Exception ex);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
        //void Debug(string message);
    }

    public interface ILog<T> : ILog where T : class { }
}
