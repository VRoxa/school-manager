using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Access.Layer.Commons;
using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Generic;
using Data.Access.Layer.Repositories.Implementations;
using Data.Access.Layer.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using School.Manager.API.Extensions;
using School.Manager.Logging;
using Serilog;

namespace School.Manager.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AllowAllCors();
            services.UseServices(Configuration);
            services.AddControllers()
                    .AddNewtonsoftJson();
            services.AddMvc();
            services.UseSwagger();

            services.AddJwtAuthentication();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILog<Startup> log)
        {
            log.Debug("Configuring application");

            log.Verbose($"Verbose test");
            log.Debug($"Debug test");

            int a = 10;
            string b = "Basura";
            log.Warn($"Warning test");

            try
            {
                throw new ArgumentException($"This is only for testing purpose ...");
            }
            catch (Exception e)
            {
                log.Error($"Error test", e);
            }

            try
            {
                throw new ArgumentException($"This is only for testing purpose ...");
            }
            catch (Exception e)
            {
                log.Fatal($"Fatal test", e);
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseSerilogRequestLogging();

            app.UseBaseSwagger();
            app.UseAllCors();
            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //await app.UseAdministratorUser();

            log.Info($"Application started. School Manager listening ...");
        }
    }
}
