﻿using Data.Access.Layer.Client;
using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Generic;
using Data.Access.Layer.Repositories.Implementations;
using Data.Access.Layer.Repositories.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using School.Manager.API.Services.Implementations;
using School.Manager.API.Services.Interfaces;
using School.Manager.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Manager.API.Extensions
{
    public static class ServicesExtension
    {
        public static void UseServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DatabaseSettings>(
               configuration.GetSection(nameof(DatabaseSettings)));

            services.AddSingleton<IDatabaseSettings>(provider =>
            {
                IOptions<DatabaseSettings> settings = provider.GetRequiredService<IOptions<DatabaseSettings>>();
                return settings.Value;
            });

            services.AddScoped(typeof(IMongoDb<>), typeof(MongoDb<>));
            services.AddScoped<IGenericRepository<Student, string>, GenericRepository<Student, string>>();
            services.AddScoped<IStudentService, StudentService>();
            //services.AddSingleton<IUserManager, UserManager>();
            //services.UseManagerLogging();

            services.AddScoped(typeof(ILog<>), typeof(GenericLog<>));
        }
    }
}
