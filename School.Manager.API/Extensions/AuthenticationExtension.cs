﻿using Data.Access.Layer.Commons;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Manager.API.Extensions
{
    public static class AuthenticationExtension
    {
        public static void AddJwtAuthentication(this IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = Constants.VALID_AUDIENCE,
                    ValidIssuer = Constants.VALID_AUDIENCE,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constants.SYMMETRIC_SECURITY_KEY))
                };
            });
        }

        public static async Task UseAdministratorUser(this IApplicationBuilder app)
        {
            IUserManager manager = app.ApplicationServices.GetRequiredService<IUserManager>();

            UserAuthentication user = (await manager.GetAll())
                                                    .FirstOrDefault(u => u.Username == "admin");
            if (user is null)
            {
                manager.RegisterUser(new UserAuthentication()
                {
                    Username = "admin",
                    Email = "admin@schoolmanager.com",
                    Password = "1"
                }).Wait();
            }
        }
    }
}
