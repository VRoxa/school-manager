﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using School.Manager.API.Services.Interfaces;

namespace School.Manager.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _service;

        public StudentsController(IStudentService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Student>>> Get()
        {
            IEnumerable<Student> students = await _service.GetAllStudents();
            return Ok(students);
        }

        [HttpPut]
        public async Task<ActionResult<Student>> AddStudent(Student student)
        {
            Student insertedStudent = await _service.AddStudent(student);
            return Ok(insertedStudent);
        }

        [HttpPatch]
        public async Task<ActionResult> UpdateStudent(Student student)
        {
            student.LastModified = DateTime.Now;

            await _service.UpdateStudent(student);
            return Ok();
        }

        [HttpPost]
        [Route("subscribe")]
        public async Task<ActionResult> SubscribeStudent(Student student)
        {
            await _service.SubscribeStudent(student);
            return Ok();
        }

        [HttpPost]
        [Route("unsubscribe")]
        public async Task<ActionResult> UnsubscribeStudent(Student student)
        {
            await _service.UnsubscribeStudent(student);
            return Ok();
        }
    }
}