﻿using Data.Access.Layer.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Manager.WebServer.AspExtensions
{
    public static class AspConfigurationExtension
    {
        public static DatabaseSettings ToDatabaseSettings(this IConfigurationSection section)
        {
            string connection = section.GetSection("ConnectionString").Value;
            string database = section.GetSection("DatabaseName").Value;
            return new DatabaseSettings
            {
                ConnectionString = connection,
                DatabaseName = database
            };
        }
    }


}
