﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace School.Manager.WebServer.AspExtensions
{
    public static class CorsExtensions
    {
        readonly static string _corsOrigins = "_managedCORS";

        public static void AllowAllCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(_corsOrigins, builder =>
                {
                    //builder.WithOrigins("http://localhost:4200");
                    builder.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                });
            });
        }

        public static void UseAllCors(this IApplicationBuilder app)
        {
            app.UseCors(_corsOrigins);
        }
    }
}
