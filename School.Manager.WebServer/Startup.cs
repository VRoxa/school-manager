using Autofac;
using School.Manager.WebServer.AspExtensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Data.Access.Layer.Models;
using Data.Access.Layer.Client;
using Data.Access.Layer.Repositories.Generic;
using Data.Access.Layer.Repositories.Implementations;
using Data.Access.Layer.Repositories.Interfaces;
using School.Manager.WebServer.Services.Implementations;
using School.Manager.WebServer.Services.Interfaces;
using School.Manager.Logging;
using Data.Access.Layer.Models.Database;
using System.Linq;

namespace School.Manager.WebServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var settings = Configuration.GetSection(nameof(DatabaseSettings)).ToDatabaseSettings();
            builder.RegisterInstance<IDatabaseSettings>(settings);

            builder.RegisterGeneric(typeof(MongoDb<>)).As(typeof(IMongoDb<>));
            builder.RegisterGeneric(typeof(GenericLog<>)).As(typeof(ILog<>));
            builder.RegisterGeneric(typeof(GenericRepository<,>)).As(typeof(IGenericRepository<,>));

            builder.RegisterType<UserManager>().As<IUserManager>();
            builder.RegisterType<StudentService>().As<IStudentService>();

            builder.RegisterBuildCallback(c =>
            {
                var manager = c.Resolve<IUserManager>();
                UserAuthentication user = manager.GetAll().Result.FirstOrDefault(u => u.Username == "admin");
                if (user is null)
                {
                    manager.RegisterUser(new UserAuthentication()
                    {
                        Username = "admin",
                        Email = "admin@schoolmanager.com",
                        Password = "1"
                    }).Wait();
                }
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddControllersWithViews();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddMvc();
            services.AllowAllCors();
            services.UseSwagger();
            services.AddJwtAuthentication();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            //if (!env.IsDevelopment())
            //{
            //}

            app.UseAllCors();

            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapControllers();
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                //if (env.IsDevelopment())
                //{
                //    spa.UseAngularCliServer(npmScript: "start");
                //}
            });

            app.UseBaseSwagger();
            app.UseAuthentication();
        }
    }
}
