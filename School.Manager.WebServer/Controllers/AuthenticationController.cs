﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Data.Access.Layer.Commons;
using Data.Access.Layer.Models;
using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using School.Manager.Logging;

namespace School.Manager.WebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IUserManager _manager;
        private readonly ILog<AuthenticationController> _log;

        public AuthenticationController(IUserManager manager, ILog<AuthenticationController> log)
        {
            _manager = manager;
            _log = log;
        }

        [HttpPost]
        public async Task<IActionResult> Authentication(LoginModel model)
        {
            _log.Info($"Authentication request as username {model.Username}");

            try
            {
                UserAuthentication user = await _manager.GetUser(model.Username);

                if (_manager.CheckValid(user, model.Password))
                {
                    var token = ResolveToken(user.Username);
                    await _manager.LogInUser(user);
                    _log.Info($"User {user.Username} authenticated");
                    return Ok(token);
                }
            }
            catch (Exception e)
            {
                _log.Error($"Authentication failure", e);
            }

            _log.Warn($"{model.Username} unauthorized");
            return Unauthorized();
        }

        [NonAction]
        public object ResolveToken(string username)
        {
            Claim[] claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            SymmetricSecurityKey authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constants.SYMMETRIC_SECURITY_KEY));

            JwtSecurityToken token = new JwtSecurityToken(
                issuer: Constants.VALID_AUDIENCE,
                audience: Constants.VALID_AUDIENCE,
                expires: DateTime.Now.AddHours(3),
                claims: claims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
            );

            return new
            {
                Token = new JwtSecurityTokenHandler()
                            .WriteToken(token),
                Expiration = token.ValidTo
            };
        }
    }
}