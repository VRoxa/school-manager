﻿using Data.Access.Layer.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace School.Manager.WebServer.Services.Interfaces
{
    public interface IStudentService
    {
        Task<IEnumerable<Student>> GetAllStudents();
        Task<IEnumerable<Student>> GetAllStudents(Expression<Func<Student, bool>> predicate);
        Task<Student> GetStudentById(string id);
        Task<Student> GetStudentByDNI(string dni);

        Task<Student> AddStudent(Student student);

        Task RemoveStudent(Student student);
        Task RemoveStudent(Expression<Func<Student, bool>> predicate);
        Task RemoveStudentById(string id);
        Task RemoveStudentByDNI(string dni);

        Task UpdateStudent(Student student);
        Task UpdateStudent(string id, Student student);

        Task SubscribeStudent(Student student);
        Task UnsubscribeStudent(Student student);
    }
}
