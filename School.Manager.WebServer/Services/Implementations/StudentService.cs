﻿using Data.Access.Layer.Models.Database;
using Data.Access.Layer.Models.Enums;
using Data.Access.Layer.Repositories.Generic;
using School.Manager.WebServer.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace School.Manager.WebServer.Services.Implementations
{
    public class StudentService : IStudentService
    {
        private readonly IGenericRepository<Student, string> _repository;

        public StudentService(IGenericRepository<Student, string> repository)
        {
            _repository = repository;
        }

        public async Task<Student> AddStudent(Student student)
        {
            student.InsertTime = student.LastModified = DateTime.Now;
            Student insertedStudent = await _repository.Create(student);
            return insertedStudent;
        }

        public async Task<IEnumerable<Student>> GetAllStudents()
        {
            return await _repository.GetAll();
        }

        public async Task<IEnumerable<Student>> GetAllStudents(Expression<Func<Student, bool>> predicate)
        {
            return await _repository.GetAll(predicate);
        }

        public async Task<Student> GetStudentByDNI(string dni)
        {
            Student student = (await _repository.GetAll(st => st.DNI == dni)).FirstOrDefault();
            return student;
        }

        public async Task<Student> GetStudentById(string id)
        {
            return await _repository.Get(id);
        }

        public async Task RemoveStudent(Student student)
        {
            await _repository.Remove(student);
        }

        public async Task RemoveStudent(Expression<Func<Student, bool>> predicate)
        {
            long count = await _repository.Remove(predicate);
        }

        public async Task RemoveStudentByDNI(string dni)
        {
            long count = await _repository.Remove(st => st.DNI == dni);
        }

        public async Task RemoveStudentById(string id)
        {
            await _repository.Remove(id);
        }

        public async Task SubscribeStudent(Student student)
        {
            student.Status = StudentStatus.Subscribed;
            student.Subscriptions.Add(DateTime.Now);

            await UpdateStudent(student);
        }

        public async Task UnsubscribeStudent(Student student)
        {
            student.Status = StudentStatus.Unsubscribed;
            student.Unsubscriptions.Add(DateTime.Now);

            await UpdateStudent(student);
        }

        public async Task UpdateStudent(Student student)
        {
            await UpdateStudent(student.Id, student);
        }

        public async Task UpdateStudent(string id, Student student)
        {
            student.LastModified = DateTime.Now;
            await _repository.Update(id, student);
        }
    }
}
