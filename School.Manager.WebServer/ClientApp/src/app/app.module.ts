import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './modules/material.module';
import { LoaderComponent } from './components/login/loader/loader.component';
import { LoginComponent } from './components/login/login.component';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MatPaginatorIntl } from '@angular/material';
import { MatPaginatorTranslator } from './translation/mat-paginator-intl';
import { MainViewComponent } from './components/main-view/main-view.component';
import { StudentsComponent } from './components/students/students.component';
import { StudentCreationDialogComponent } from './components/students/student-creation-dialog/student-creation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    LoginComponent,
    LoaderComponent,
    MainViewComponent,
    StudentsComponent,
    StudentCreationDialogComponent
  ],
  entryComponents: [
    StudentCreationDialogComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http),
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot([
      { path: '', component: MainViewComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'login', component: LoginComponent },
      { path: 'students', component: StudentsComponent }
    ]),
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    {
      provide: MatPaginatorIntl,
      deps: [TranslateService],
      useFactory: (translateService: TranslateService) => new MatPaginatorTranslator(translateService)
    }
  ],
  bootstrap: [MainViewComponent]
})
export class AppModule { }
