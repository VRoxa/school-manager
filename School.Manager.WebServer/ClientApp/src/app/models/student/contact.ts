export class Contact {
    phoneNumber: string;
    cellphoneNumber: string;
    email: string;
}
