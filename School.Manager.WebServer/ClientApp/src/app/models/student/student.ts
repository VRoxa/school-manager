import { Address } from './address';
import { Contact } from './contact';
import { PaymentData } from './payment-data';
import { StudentStatus } from '../enums/student-status.enum';
import { Receipt } from './receipt';

export class Student {
    id: string;

    name: string;
    surname: string;
    lastName: string;
    DNI: string;

    birthdate: Date;

    address: Address = new Address();
    contact: Contact = new Contact();

    paymentData: PaymentData = new PaymentData();
    status: StudentStatus;

    about: string;

    subscriptions: Date[];
    unsubscriptions: Date[];
    insertTime: Date;

    receipts: Receipt[] = [];

    // Remove under production env
    // constructor() {
    //     this.name = this.surname = this.lastName = 'a';
    //     this.birthdate = new Date();
    // }
}
