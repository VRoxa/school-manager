import { PaymentType } from '../enums/payment-type.enum';

export class PaymentData {
    bankAccount: string;
    paymentType: PaymentType;
}
