import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from 'src/app/models/login-model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpAuthenticatedService } from '../http-authenticated/http-authenticated.service';
import { AuthenticationResponse } from 'src/app/models/authentication-response';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _url: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string, private auth: HttpAuthenticatedService) {
    this._url = `${baseUrl}/authentication`;
  }

  authenticate = (model: LoginModel): Observable<AuthenticationResponse> => {
    return this.http.post<AuthenticationResponse>(this._url, model)
                    .pipe(map((response: AuthenticationResponse) => {
                      this.auth.setToken(response.token);
                      return response;
                    }));
  }
}
