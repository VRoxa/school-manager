import { TestBed } from '@angular/core/testing';

import { HttpAuthenticatedService } from './http-authenticated.service';

describe('HttpAuthenticatedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpAuthenticatedService = TestBed.get(HttpAuthenticatedService);
    expect(service).toBeTruthy();
  });
});
