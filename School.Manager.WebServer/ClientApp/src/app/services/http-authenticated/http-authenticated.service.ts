import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpAuthenticatedService {
  private static headers: HttpHeaders;
  private static onAuthenticated: ReplaySubject<void> = new ReplaySubject<void>();
  private _headers: HttpHeaders;

  constructor(private http: HttpClient, @Inject('BASE_URL') private _api: string) {
    this._headers = HttpAuthenticatedService.headers;
  }

  public get = <TResponse>(url: string): Observable<TResponse> =>
    this.http.get<TResponse>(`${this._api}/${url}`, { headers: this._headers })

  public post = <TResponse>(url: string, body?: any): Observable<TResponse> =>
    this.http.post<TResponse>(`${this._api}/${url}`, body, { headers: this._headers })

  public put = <TResponse>(url: string, body?: any): Observable<TResponse> =>
    this.http.put<TResponse>(`${this._api}/${url}`, body, { headers: this._headers })

  public patch = <TResponse>(url: string, body?: any): Observable<TResponse> =>
    this.http.patch<TResponse>(`${this._api}/${url}`, body, { headers: this._headers })

  public delete = <TResponse>(url: string): Observable<TResponse> =>
    this.http.delete<TResponse>(`${this._api}/${url}`, { headers: this._headers })

  public setToken = (token: string): void => {
    this._headers = HttpAuthenticatedService.headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'Content-Type': 'application/json'
    });

    HttpAuthenticatedService.onAuthenticated.next();
  }

  public isAuthenticated = (): boolean =>
    this._headers !== undefined && this._headers !== null

  get onAuthenticated(): ReplaySubject<void> {
    return HttpAuthenticatedService.onAuthenticated;
  }
}
