import { Injectable } from '@angular/core';
import { HttpAuthenticatedService } from '../http-authenticated/http-authenticated.service';
import { Observable } from 'rxjs';
import { Student } from 'src/app/models/student/student';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  private _apiSuffix = 'students';

  constructor(private http: HttpAuthenticatedService) { }

  getStudents = (): Observable<Student[]> => {
    return this.http.get<Student[]>(this._apiSuffix);
  }

  addStudent = (student: Student): Observable<Student> => {
    return this.http.put<Student>(this._apiSuffix, student);
  }

  updateStudent = (student: Student): Observable<Student> => {
    return this.http.patch<Student>(this._apiSuffix, student);
  }

  subscribeStudent = (student: Student): Observable<Student> => {
    return this.http.post<Student>(`${this._apiSuffix}/subscribe`, student);
  }

  unsubscribeStudent = (student: Student): Observable<Student> => {
    return this.http.post<Student>(`${this._apiSuffix}/unsubscribe`, student);
  }
}
