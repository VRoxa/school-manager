import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ReplaySubject } from 'rxjs';
import { LoginModel } from 'src/app/models/login-model';
import { MatIcon, MatInput, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = true;
  asAdmin = true;
  stopLoading: ReplaySubject<void> = new ReplaySubject<void>();

  credentials: LoginModel = {
    username: '',
    password: ''
  };

  @ViewChild('passwordInput', {static: false}) input: ElementRef<MatInput>;
  @ViewChild('iconPassword', {static: false}) icon: any;

  constructor(
    private service: AuthenticationService,
    private route: Router,
    private snack: MatSnackBar) { }

  ngOnInit() {
    setTimeout(() => {
      this.stopLoading.next();

      // Only in testing environment
      this.credentials = {
        password: '1',
        username: 'admin'
      };

      // this.authenticate();
    }, 20);
  }

  login = () => {
    if (this.asAdmin) {
      this.authenticate();
    } else {
      this.snack.open('Feature not implemented yet...', 'Ok', {duration: 2000});
    }
  }

  authenticate = () => {
    this.service
        .authenticate(this.credentials)
        .subscribe(
          response => this.route.navigate(['/students']),
          err => {
            this.snack.open('Authentication failed', 'Ok', {duration: 2000});
            console.error(err);
          }
        );
  }

  onLoadingAnimationEnd = () => {
    this.loading = false;
  }

  showPassword = () => {
    this.input.nativeElement.type = 'text';
    this.input.nativeElement.focused = true;

    const icon = this.icon as MatIcon;
    icon._elementRef.nativeElement.innerHTML = 'visibility';
  }

  hidePassword = () => {
    this.input.nativeElement.type = 'password';

    const icon = this.icon as MatIcon;
    icon._elementRef.nativeElement.innerHTML = 'visibility_off';
  }
}
