import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HttpAuthenticatedService } from 'src/app/services/http-authenticated/http-authenticated.service';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements OnInit {
  public authenticated: boolean;
  @ViewChild('drawer', {static: false}) drawer;
  public activeLanguage: string;

  constructor(
    private router: Router,
    private auth: HttpAuthenticatedService, 
    private translateService: TranslateService) { }

  ngOnInit() {
    const defaultLang = localStorage.getItem('sch_active_language');
    this.activeLanguage = defaultLang ? defaultLang : 'en';

    this.translateService.setDefaultLang(this.activeLanguage);

    this.auth.onAuthenticated.subscribe(_ => this.authenticated = true);
    this.authenticated = this.auth.isAuthenticated();

    if (!this.authenticated) {
      this.router.navigate(['/login']);
    }
  }

  selectLanguage = () => {
    localStorage.setItem('sch_active_language', this.activeLanguage);
    this.translateService.use(this.activeLanguage);
  }

}
