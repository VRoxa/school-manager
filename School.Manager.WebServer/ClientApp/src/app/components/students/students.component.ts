import { Component, OnInit, ViewChild } from '@angular/core';
import { Student } from 'src/app/models/student/student';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { StudentCreationDialogComponent } from './student-creation-dialog/student-creation-dialog.component';
import { StudentStatus } from 'src/app/models/enums/student-status.enum';
import { StudentsService } from 'src/app/services/students/students.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  dataSource: MatTableDataSource<Student>;
  columns: string[] = ['name', 'surname', 'lastName', 'DNI', 'birthdate'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private service: StudentsService, private dialog: MatDialog) { }

  ngOnInit() {
    this.service.getStudents().subscribe(students => {
      this.dataSource = new MatTableDataSource(students);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  addStudent = (): void => {
    const dialog = this.dialog.open(StudentCreationDialogComponent, {
      width: '80%',
      maxWidth: '80%',
      disableClose: true,
      autoFocus: false
    });

    dialog.afterClosed().subscribe(student => {
      if (student) {
        this.createStudent(student);
      }
    });
  }

  createStudent = (student: Student) => {
    // this.service.addStudent(student).subscribe((created: Student) => {
      // this.dataSource.data.push(created);
    // });

    console.log('To create...', student);
  }
}
