import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSlideToggleChange, MatButtonToggleChange } from '@angular/material';
import { Student } from 'src/app/models/student/student';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { PaymentType } from 'src/app/models/enums/payment-type.enum';
import { validateHorizontalPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'app-student-creation-dialog',
  templateUrl: './student-creation-dialog.component.html',
  styleUrls: ['./student-creation-dialog.component.scss']
})
export class StudentCreationDialogComponent implements OnInit {
  public student: Student;

  public firstForm: FormGroup;
  public secondForm: FormGroup;

  constructor(private dialog: MatDialogRef<StudentCreationDialogComponent>, private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.student = new Student();

    this.firstForm = this._formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      last_name: ['', Validators.required],
      birthdate: ['', Validators.required],
      dni: ['', Validators.required]
    });

    this.secondForm = this._formBuilder.group({
      postal_address: ['', Validators.nullValidator],
      postal_code: ['', Validators.nullValidator],
      phone: ['', Validators.nullValidator],
      cellphone: ['', Validators.nullValidator],
      email: ['', Validators.email],
      payment_type: ['', Validators.nullValidator],
      bank_account: ['', Validators.nullValidator]
    });
  }

  cancel = () => this.dialog.close();
  confirm = () => this.dialog.close(this.student);

  paymentTypeChanged = (change: MatSlideToggleChange) => {
    const type: PaymentType = change.checked
                                ? PaymentType.Bank
                                : PaymentType.Cash;
    this.secondForm.patchValue({payment_type: type});
    this.logStudent();
  }

  isPaymentBank = () => {
    return this.student.paymentData.paymentType === PaymentType.Bank;
  }

  logStudent = () => {
    console.log(this.student);
  }
}
