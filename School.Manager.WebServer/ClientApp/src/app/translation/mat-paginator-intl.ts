import { MatPaginatorIntl } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

export class MatPaginatorTranslator extends MatPaginatorIntl {
    rangeSeparation: string;
    itemsPerPageLabel = 'Items per page';
    nextPageLabel     = 'Next page';
    previousPageLabel = 'Previous page';
    getRangeLabel = (page: number, pageSize: number, length: number) => {
        if (length === 0 || pageSize === 0) { return `0 ${this.rangeSeparation} ${length}`; }

        length = Math.max(length, 0);
        const startIndex = page * pageSize;

        const endIndex = startIndex < length
                         ? Math.min(startIndex + pageSize, length)
                         : startIndex + pageSize;

        return `${startIndex + 1} - ${endIndex} ${this.rangeSeparation} ${length}`;
    }

    constructor(private service: TranslateService) {
        super();

        this.service.onLangChange.subscribe(() => {
            this.getTranslation();
        });

        this.getTranslation();
    }

    public getTranslation() {
        const prefix = 'material.paginator';

        this.itemsPerPageLabel = this.service.instant(`${prefix}.items_per_page`);
        this.nextPageLabel = this.service.instant(`${prefix}.next_page`);
        this.previousPageLabel = this.service.instant(`${prefix}.previous_page`);
        this.rangeSeparation = this.service.instant(`${prefix}.range_separation`);

        this.changes.next();
    }
}